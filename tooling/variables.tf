# AWS configuration
###################

variable "aws_profile" {}
variable "aws_region" {}
variable "environment" {}
variable "vpccidr" {}
variable "instance_type" {}
variable "key_name" {}
variable "vpc_name" {}
variable "private_subnet_name_1" {}
variable "private_subnet_name_2" {}
variable "public_subnet_name_1" {}
variable "public_subnet_name_2" {}


