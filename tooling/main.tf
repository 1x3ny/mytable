#data "aws_iam_account_alias" "current" {}

data "aws_caller_identity" "account" {}

module "network" {
  source          = "../modules/network"
  vpccidr         = "${var.vpccidr}"
  environment     = "${var.environment}"
  aws_region      = "${var.aws_region}"
  subnet_bits     = "3"
  subnet_app_bits = "2"
  azs             = "2"
}


module "bastion" {
  source                = "../modules/bastion"
  environment           = "${var.environment}"
  application           = "bastion"
  instance_type         = "${var.instance_type}"
  key_name              = "${var.key_name}"
  aws_region            = "${var.aws_region}"
  vpc_name              = "${var.vpc_name}"
  private_subnet_name_1 = "${var.private_subnet_name_1}"
  private_subnet_name_2 = "${var.private_subnet_name_2}"
  public_subnet_name_1  = "${var.public_subnet_name_1}"
  public_subnet_name_2  = "${var.public_subnet_name_2}"
  vpccidr               = "${var.vpccidr}"
}
/* 

module "bucket" {
  source        = "../modules/bucket"
  environment   = "${var.environment}"
  aws_region    = "${var.aws_region}"
  account_alias = "${data.aws_iam_account_alias.current.account_alias}"
  account_id    = "${data.aws_caller_identity.account.account_id}"
}

module "jenkins" {
  source                = "../modules/instance"
  environment           = "${var.environment}"
  application           = "jenkins"
  instance_type         = "t3.large"
  spot_price            = "0.032"
  key_name              = "${var.key_name}"
  aws_region            = "${var.aws_region}"
  vpc_name              = "${var.vpc_name}"
  private_subnet_name_1 = "${var.private_subnet_name_1}"
  private_subnet_name_2 = "${var.private_subnet_name_2}"
  public_subnet_name_1  = "${var.public_subnet_name_1}"
  public_subnet_name_2  = "${var.public_subnet_name_2}"
}

module "jenkins-volume" {
  source            = "../modules/volume"
  environment       = "${var.environment}"
  application       = "jenkins"
  aws_region        = "${var.aws_region}"
  size              = "10"
  device_name       = "/dev/sdh"
  instance_id       = "${module.jenkins.instance_id}"
  availability_zone = "${var.aws_region}b"
}
 */