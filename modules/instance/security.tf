# Create security group
resource "aws_security_group" "front_sg" {
  name        = "${var.application}"
  description = "Security group for ${var.application}"
  vpc_id      = "${data.aws_vpc.vpc.id}"

  tags {
    Name        = "${var.application}"
    Environment = "${var.environment}"
  }
}

resource "aws_security_group" "lb_sg" {
  name        = "${var.application}-lb"
  description = "Security group for ${var.application}"
  vpc_id      = "${data.aws_vpc.vpc.id}"

  tags {
    Name        = "${var.application}-lb"
    Environment = "${var.environment}"
  }
}

data "aws_security_group" "bastion_sg" {
  tags {
    Name = "bastion"
  }
}

resource "aws_security_group_rule" "front_allow_bastion" {
  type                     = "ingress"
  from_port                = 22
  to_port                  = 22
  protocol                 = "tcp"
  source_security_group_id = "${data.aws_security_group.bastion_sg.id}"
  description              = "Bastion"

  security_group_id = "${aws_security_group.front_sg.id}"
}

resource "aws_security_group_rule" "front_allow_internet_1" {
  type        = "egress"
  from_port   = 80
  to_port     = 80
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  description = "Internet"

  security_group_id = "${aws_security_group.front_sg.id}"
}

resource "aws_security_group_rule" "front_allow_internet_2" {
  type        = "egress"
  from_port   = 443
  to_port     = 443
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  description = "Internet"

  security_group_id = "${aws_security_group.front_sg.id}"
}

resource "aws_security_group_rule" "lb_allow_internet_1" {
  type        = "ingress"
  from_port   = 80
  to_port     = 80
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  description = "Internet"

  security_group_id = "${aws_security_group.lb_sg.id}"
}

resource "aws_security_group_rule" "lb_allow_internet_2" {
  type        = "ingress"
  from_port   = 443
  to_port     = 443
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  description = "Internet"

  security_group_id = "${aws_security_group.lb_sg.id}"
}

resource "aws_security_group_rule" "lb_allow_front" {
  type                     = "egress"
  from_port                = 80
  to_port                  = 80
  protocol                 = "tcp"
  source_security_group_id = "${aws_security_group.front_sg.id}"
  description              = "Front"

  security_group_id = "${aws_security_group.lb_sg.id}"
}

resource "aws_security_group_rule" "front_allow_lb" {
  type                     = "ingress"
  from_port                = 80
  to_port                  = 80
  protocol                 = "tcp"
  source_security_group_id = "${aws_security_group.lb_sg.id}"
  description              = "Loadbalancer"

  security_group_id = "${aws_security_group.front_sg.id}"
}