data "aws_acm_certificate" "certificate" {
  domain      = "*.cloud4dev.net"
  types       = ["AMAZON_ISSUED"]
  most_recent = true
}