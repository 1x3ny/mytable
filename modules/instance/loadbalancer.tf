# Create a new load balancer
resource "aws_alb" "alb" {
  name            = "${var.application}"
  internal        = false
  security_groups = ["${aws_security_group.lb_sg.id}"]
  subnets         = ["${data.aws_subnet.public_subnet_1.id}", "${data.aws_subnet.public_subnet_2.id}"]

  tags {
    Environment = "${var.environment}"
  }
}
