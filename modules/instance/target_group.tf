resource "aws_alb_target_group" "target_group" {
  name     = "${var.application}"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${data.aws_vpc.vpc.id}"

#   health_check {
#     matcher = 403
#   }

  tags {
    Environment = "${var.environment}"
    Application = "${var.application}"
  }
}

resource "aws_alb_target_group_attachment" "target_group_attachment" {
  target_group_arn = "${aws_alb_target_group.target_group.arn}"
  target_id        = "${aws_spot_instance_request.instance.spot_instance_id}"
  port             = 80

  depends_on = ["aws_spot_instance_request.instance"]
}