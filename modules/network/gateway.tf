# Create internet gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags = {
    Name        = "${var.environment}"
    Environment = "${var.environment}"
  }
}

# Nat gateway
resource "aws_nat_gateway" "nat_gateway" {
  count         = "${var.azs}"
  allocation_id = "${aws_eip.nat_public_ip.*.id[count.index]}"
  subnet_id     = "${aws_subnet.public_subnets.*.id[count.index]}"

  tags = {
    Name        = "${format("${var.environment}-%d", count.index)}"
    Environment = "${var.environment}"
  }
}
