resource "aws_vpc" "vpc" {
  cidr_block = "${var.vpccidr}"

  tags = {
    Name        = "vpc-${var.environment}"
    Environment = "${var.environment}"
  }
}