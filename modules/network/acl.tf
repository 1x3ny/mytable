resource "aws_network_acl" "public_nacl" {
  vpc_id     = "${aws_vpc.vpc.id}"
  subnet_ids = ["${aws_subnet.public_subnets.*.id}"]

  egress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  ingress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  tags {
    Name        = "public"
    Environment = "${var.environment}"
  }
}

resource "aws_network_acl" "private_application_nacl" {
  count      = "${var.azs}"
  vpc_id     = "${aws_vpc.vpc.id}"
  subnet_ids = ["${aws_subnet.private_application_subnets.*.id[count.index]}"]

  egress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  ingress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  tags = {
    Name        = "${format("private-application-${var.environment}-%d", count.index)}"
    Environment = "${var.environment}"
  }
}

resource "aws_network_acl" "private_database_nacl" {
  count      = "${var.azs}"
  vpc_id     = "${aws_vpc.vpc.id}"
  subnet_ids = ["${aws_subnet.private_database_subnets.*.id[count.index]}"]

  egress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  ingress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  tags = {
    Name        = "${format("private-database-${var.environment}-%d", count.index)}"
    Environment = "${var.environment}"
  }
}