# Nat gateway's public ip
resource "aws_eip" "nat_public_ip" {
  count = "${var.azs}"
  vpc   = true

  tags = {
    Name        = "${format("${var.environment}-%d", count.index)}"
    Environment = "${var.environment}"
  }
}