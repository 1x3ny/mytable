variable "aws_region" {}
variable "vpccidr" {}
variable "environment" {}
variable "azs" {}

variable "subnet_bits" {
  description = "The number of bits used for subnet addressing."
}

variable "subnet_app_bits" {
  description = "The number of bits used for subnet app addressing."
}
 
variable "subnet_numbers_beginning" {
  type        = "map"
  description = "The position from which subnet numbers begin for a given subnet type."

  default {
    public_subnets              = "0"
    private_application_subnets = "1"
    private_database_subnets    = "6"
  }
} 
