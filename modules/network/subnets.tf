# Public dmz Subnets
resource "aws_subnet" "public_subnets" {
  count             = "${var.azs}"
  vpc_id            = "${aws_vpc.vpc.id}"
  cidr_block        = "${cidrsubnet("${var.vpccidr}", "${var.subnet_bits}", lookup(var.subnet_numbers_beginning, "public_subnets")+count.index)}"
  availability_zone = "${data.aws_availability_zones.azs.names[count.index]}"

  tags = {
    Name        = "${format("public-${var.environment}-%d", count.index)}"
    Environment = "${var.environment}"
  }
}

# Public dmz subnets route tables
resource "aws_route_table" "public_route_table" {
  vpc_id = "${aws_vpc.vpc.id}"

  # Igw routing
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.igw.id}"
  }

  tags = {
    Name        = "public-${var.environment}"
    Environment = "${var.environment}"
  }
}

resource "aws_route_table_association" "public_route_tables_association" {
  count          = "${var.azs}"
  subnet_id      = "${aws_subnet.public_subnets.*.id[count.index]}"
  route_table_id = "${aws_route_table.public_route_table.id}"
}

# Private application subnets
resource "aws_subnet" "private_application_subnets" {
  count             = "${var.azs}"
  vpc_id            = "${aws_vpc.vpc.id}"
  cidr_block        = "${cidrsubnet("${var.vpccidr}", "${var.subnet_app_bits}", lookup(var.subnet_numbers_beginning, "private_application_subnets")+count.index)}"
  availability_zone = "${data.aws_availability_zones.azs.names[count.index]}"

  tags = {
    Name        = "${format("private-application-${var.environment}-%d", count.index)}"
    Environment = "${var.environment}"
  }

  depends_on = ["aws_subnet.public_subnets"]
}

# Private application subnets route tables
resource "aws_route_table" "private_application_route_tables" {
  count  = "${var.azs}"
  vpc_id = "${aws_vpc.vpc.id}"

  # nat gateway routing
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_nat_gateway.nat_gateway.*.id[count.index]}"
  }

  tags = {
    Name        = "${format("private-application-${var.environment}-%d", count.index)}"
    Environment = "${var.environment}"
  }
}

resource "aws_route_table_association" "private_application_route_tables_association" {
  count          = "${var.azs}"
  subnet_id      = "${aws_subnet.private_application_subnets.*.id[count.index]}"
  route_table_id = "${aws_route_table.private_application_route_tables.*.id[count.index]}"
}

# Private database subnets
resource "aws_subnet" "private_database_subnets" {
  count             = "${var.azs}"
  vpc_id            = "${aws_vpc.vpc.id}"
  cidr_block        = "${cidrsubnet("${var.vpccidr}", "${var.subnet_bits}", lookup(var.subnet_numbers_beginning, "private_database_subnets")+count.index)}"
  availability_zone = "${data.aws_availability_zones.azs.names[count.index]}"

  tags = {
    Name        = "${format("private-database-${var.environment}-%d", count.index)}"
    Environment = "${var.environment}"
  }

  depends_on = ["aws_subnet.private_application_subnets"]
}

# Private database subnets route tables
resource "aws_route_table" "private_database_route_tables" {
  count  = "${var.azs}"
  vpc_id = "${aws_vpc.vpc.id}"

  tags = {
    Name        = "${format("private-database-${var.environment}-%d", count.index)}"
    Environment = "${var.environment}"
  }
}

resource "aws_route_table_association" "private_database_route_tables_association" {
  count          = "${var.azs}"
  subnet_id      = "${aws_subnet.private_database_subnets.*.id[count.index]}"
  route_table_id = "${aws_route_table.private_database_route_tables.*.id[count.index]}"
}
