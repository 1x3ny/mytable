# Create security group
resource "aws_security_group" "front_sg" {
  name        = "${var.application}"
  description = "Security group for ${var.application}"
  vpc_id      = "${data.aws_vpc.vpc.id}"

  tags {
    Name        = "${var.application}"
    Environment = "${var.environment}"
  }
}

resource "aws_security_group_rule" "front_allow_nlb_1" {
  type        = "ingress"
  from_port   = 80
  to_port     = 80
  protocol    = "tcp"
  cidr_blocks = ["${data.aws_network_interface.nlb_eni_1.private_ip}/32"]
  description = "nlb"

  security_group_id = "${aws_security_group.front_sg.id}"
}

resource "aws_security_group_rule" "front_allow_nlb_2" {
  type        = "ingress"
  from_port   = 80
  to_port     = 80
  protocol    = "tcp"
  cidr_blocks = ["${data.aws_network_interface.nlb_eni_2.private_ip}/32"]
  description = "nlb"

  security_group_id = "${aws_security_group.front_sg.id}"
}

resource "aws_security_group_rule" "front_allow_internet_1" {
  type        = "egress"
  from_port   = 80
  to_port     = 80
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  description = "Internet"

  security_group_id = "${aws_security_group.front_sg.id}"
}

resource "aws_security_group_rule" "front_allow_internet_2" {
  type        = "egress"
  from_port   = 443
  to_port     = 443
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  description = "Internet"

  security_group_id = "${aws_security_group.front_sg.id}"
}

resource "aws_security_group_rule" "front_allow_ssh" {
  type        = "ingress"
  from_port   = 22
  to_port     = 22
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  description = "Internet"

  security_group_id = "${aws_security_group.front_sg.id}"
}

resource "aws_security_group_rule" "bastion_allow_ssh" {
  type        = "egress"
  from_port   = 22
  to_port     = 22
  protocol    = "tcp"
  cidr_blocks = ["${var.vpccidr}"]
  description = "${var.environment}"

  security_group_id = "${aws_security_group.front_sg.id}"
}