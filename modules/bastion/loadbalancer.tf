# Create a new load balancer
resource "aws_alb" "nlb" {
  name               = "${var.application}"
  internal           = false
  load_balancer_type = "network"
  subnets            = ["${data.aws_subnet.public_subnet_1.id}", "${data.aws_subnet.public_subnet_2.id}"]

  tags {
    Environment = "${var.environment}"
    Application = "${var.application}"
  }
}

resource "aws_lb_target_group" "target_group" {
  name     = "${var.application}"
  port     = 22
  protocol = "TCP"
  vpc_id   = "${data.aws_vpc.vpc.id}"

  health_check {
    port     = "80"
    protocol = "HTTP"
  }

  tags {
    Environment = "${var.environment}"
    Application = "${var.application}"
  }
}

resource "aws_lb_target_group_attachment" "target_group_attachment" {
  target_group_arn = "${aws_lb_target_group.target_group.arn}"
  target_id        = "${aws_spot_instance_request.instance.spot_instance_id}"
  port             = 22
}

resource "aws_lb_listener" "listener" {
  load_balancer_arn = "${aws_alb.nlb.arn}"
  port              = "22"
  protocol          = "TCP"

  default_action {
    target_group_arn = "${aws_lb_target_group.target_group.arn}"
    type             = "forward"
  }
}
