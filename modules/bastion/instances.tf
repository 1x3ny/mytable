data "aws_ami" "ami" {
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*gp2"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  owners      = ["amazon"]
  most_recent = true
}

resource "aws_spot_instance_request" "instance" {
  ami           = "${data.aws_ami.ami.id}"
  instance_type = "${var.instance_type}"
  key_name      = "${var.key_name}"
  spot_price    = "0.03"

  network_interface {
    network_interface_id = "${aws_network_interface.eni.id}"
    device_index         = 0
  }

  root_block_device {
    volume_type = "gp2"
    volume_size = 8
  }

  volume_tags {
    Name        = "${var.application}"
    Environment = "${var.environment}"
  }

  tags {
    Name        = "${var.application}"
    Environment = "${var.environment}"
  }
}
