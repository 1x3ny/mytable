data "aws_vpc" "vpc" {
  tags {
    Name = "${var.vpc_name}"
  }
}

data "aws_subnet" "private_subnet_1" {
  tags {
    Name = "${var.private_subnet_name_1}"
  }
}

data "aws_subnet" "private_subnet_2" {
  tags {
    Name = "${var.private_subnet_name_2}"
  }
}

data "aws_subnet" "public_subnet_1" {
  tags {
    Name = "${var.public_subnet_name_1}"
  }
}

data "aws_subnet" "public_subnet_2" {
  tags {
    Name = "${var.public_subnet_name_2}"
  }
}

resource "aws_network_interface" "eni" {
  description     = "Network Interface for ${var.application}"
  subnet_id       = "${data.aws_subnet.private_subnet_2.id}"
  security_groups = ["${aws_security_group.front_sg.id}"]

  tags {
    Name        = "${var.application}"
    Environment = "${var.environment}"
  }
}

data "aws_network_interface" "nlb_eni_1" {
  filter = {
    name   = "description"
    values = ["ELB ${aws_alb.nlb.arn_suffix}"]
  }

  filter = {
    name   = "availability-zone"
    values = ["eu-west-1a"]
  }
}

data "aws_network_interface" "nlb_eni_2" {
  filter = {
    name   = "description"
    values = ["ELB ${aws_alb.nlb.arn_suffix}"]
  }

  filter = {
    name   = "availability-zone"
    values = ["eu-west-1b"]
  }
}
