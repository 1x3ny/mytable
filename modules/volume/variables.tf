variable "aws_region" {}
variable "application" {}
variable "environment" {}
variable "instance_id" {}
variable "size" {}
variable "device_name" {}
variable "availability_zone" {}