resource "aws_ebs_volume" "volume" {
  availability_zone = "${var.availability_zone}"
  size              = "${var.size}"
  type              = "gp2"

  tags = {
    Name        = "${var.application}-data"
    Environment = "${var.environment}"
  }
}

resource "aws_volume_attachment" "volume_att" {
  device_name = "${var.device_name}"
  volume_id   = "${aws_ebs_volume.volume.id}"
  instance_id = "${var.instance_id}"
}
