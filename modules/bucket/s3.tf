resource "aws_s3_bucket" "logs_bucket" {
  bucket = "${var.account_alias}-bucket-logs"
  acl    = "log-delivery-write"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  versioning {
    enabled = true
  }

  lifecycle_rule {
    id      = "cleanup"
    enabled = true

    expiration {
      days = 14
    }

    noncurrent_version_expiration {
      days = 1
    }
  }

  tags = {
    Name        = "${var.account_alias}-bucket-logs"
    Environment = "${var.environment}"
  }
}

resource "aws_s3_bucket_public_access_block" "logs_bucket_access_block" {
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true

  bucket = "${aws_s3_bucket.logs_bucket.id}"
}

resource "aws_s3_bucket" "alb_logs_bucket" {
  bucket = "${var.account_alias}-alb-logs"
  acl    = "private"

  policy = <<POLICY
{
        "Version": "2012-10-17",
        "Id": "AWSConsole-AccessLogs-Policy",
        "Statement": [
            {
                "Effect": "Allow",
                "Sid": "AWSConsoleStmt",
                "Principal": {
                    "AWS": "arn:aws:iam::${var.account_id}:root"
                },
                "Action": "s3:PutObject",
                "Resource": "arn:aws:s3:::${var.account_alias}-alb-logs/*"
            },
            {
                "Sid": "AWSLogDeliveryWrite",
                "Effect": "Allow",
                "Principal": {
                    "Service": "delivery.logs.amazonaws.com"
                },
                "Action": "s3:PutObject",
                "Resource": "arn:aws:s3:::${var.account_alias}-alb-logs/*",
                "Condition": {
                    "StringEquals": {
                        "s3:x-amz-acl": "bucket-owner-full-control"
                    }
                }
            },
            {
                "Sid": "AWSLogDeliveryAclCheck",
                "Effect": "Allow",
                "Principal": {
                    "Service": "delivery.logs.amazonaws.com"
                },
                "Action": "s3:GetBucketAcl",
                "Resource": "arn:aws:s3:::${var.account_alias}-alb-logs"
            }
        ]
    }
POLICY

  versioning {
    enabled = true
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  logging {
    target_bucket = "${aws_s3_bucket.logs_bucket.id}"
    target_prefix = "alb-logs/"
  }

  lifecycle_rule {
    id      = "cleanup"
    enabled = true

    expiration {
      days = 14
    }

    noncurrent_version_expiration {
      days = 1
    }
  }

  tags = {
    Name        = "${var.account_alias}-logs"
    Environment = "${var.environment}"
  }
}

resource "aws_s3_bucket_public_access_block" "alb_logs_bucket_access_block" {
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true

  bucket = "${aws_s3_bucket.alb_logs_bucket.id}"
}